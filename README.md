# PokemonTrainer
Visit the app [here](https://pokemon-trainer-anton.herokuapp.com)!

On the home page you can enter your name to start your Pokemon trainer career. This name is saved in localstorage so you can continue training until you clear your cache.

On the `/pokemon` page you can browse all available pokemon and click on their card to see their details. If you like the Pokemon click the `Collect Pokemon` button.

On the `/trainer` page you can see all the Pokemon you have collected. Clicking a Pokemon will take you to their details page. If you don't like the Pokemon anymore you can let it go by clicking the `Let Go` button.

## Project Details
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.11.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
