import { Component } from '@angular/core';
import { User } from './_models/user.model';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private readonly authService: AuthService) {}

  get user(): User {
    return this.authService.user();
  }

  get isLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }
}
