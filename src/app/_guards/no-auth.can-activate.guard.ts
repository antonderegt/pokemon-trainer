import { Injectable } from '@angular/core';
import {  CanActivate, Router } from "@angular/router";
import { AuthService } from "../services/auth.service";

@Injectable({
    providedIn: 'root'
})
export class NoAuthCanActivateGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private readonly router: Router
    ) { }

    canActivate() {
        const isLoggedIn: boolean = this.authService.isLoggedIn();

        if (!isLoggedIn) {
            return true;
        }

        this.router.navigate(['/pokemon']);
        return false;
    }
}