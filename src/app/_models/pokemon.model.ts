export interface PokemonList {
    count?: number,
    next: string,
    previous: string,
    results?: Pokemon[]
}

export interface Pokemon {
    name: string,
    url?: string,
    abilities?: Ability[],
    base_experience?: number,
    forms?: [],
    game_indices?: [],
    height?: number,
    held_items?: [],
    id?: number,
    is_default?: boolean,
    location_area_encounters?: string,
    moves?: Move[],
    order?: number,
    past_types?: [],
    species?: {},
    sprites?: Sprites,
    stats?: Stat[],
    types?: Type[],
    weight?: number
}

interface Ability {
    ability: {
        name: string,
        url: string
    },
    is_hidded: boolean,
    slot: number
}

interface Move {
    move: {
        name: string,
        url: string
    }
}

interface Sprites {
    back_default: string | null
    back_female: string | null
    back_shiny: string | null
    back_shiny_female: string | null
    front_default: string | null
    front_female: string | null
    front_shiny: string | null
    front_shiny_female: string | null
}

interface Type {
    slot: number,
    type: {
        name: string,
        url: string
    }
}

interface Stat {
    base_stat: number,
    effort: number,
    stat: {
        name: string,
        url: string
    }
}