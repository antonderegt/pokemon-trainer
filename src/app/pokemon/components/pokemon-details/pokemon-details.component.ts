import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Pokemon } from 'src/app/_models/pokemon.model';
import { CollectPokemonService } from 'src/app/services/collect-pokemon.service';
import { PokemonService } from 'src/app/services/pokemon.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-pokemon-details',
  templateUrl: './pokemon-details.component.html',
  styleUrls: ['./pokemon-details.component.scss']
})
export class PokemonDetailsComponent implements OnInit {
  constructor(
    private route: ActivatedRoute,
    private readonly pokemonService: PokemonService,
    private readonly collectPokemonService: CollectPokemonService,
    private readonly loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe(({ id }) => {
      this.pokemonService.fetchPokemonDetailsById(Number(id));
      this.collectPokemonService.loadCollectedPokemon();
    })
  }

  get pokemon(): Pokemon | undefined {
    return this.pokemonService.selectedPokemon();
  }

  get error(): string {
    return this.pokemonService.error();
  }

  get loading(): boolean {
    return this.loadingService.loading();
  }

  get isCollected(): boolean {
    return this.collectPokemonService.isCollected(this.pokemon?.name);
  }

  onCollectClick(): void {
    this.collectPokemonService.collectPokemon(this.pokemon);
  }

  onLetGoClick(): void {
    this.collectPokemonService.letGoPokemon(this.pokemon?.name);
  }
}
