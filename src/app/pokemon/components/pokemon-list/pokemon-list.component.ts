import { Component, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/_models/pokemon.model';
import { CollectPokemonService } from 'src/app/services/collect-pokemon.service';
import { PokemonService } from 'src/app/services/pokemon.service';
import { LoadingService } from 'src/app/services/loading.service';
import { Url } from '../../../_utils/url.util';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
  constructor(
    private readonly pokemonService: PokemonService,
    private readonly collectPokemonService: CollectPokemonService,
    private readonly loadingService: LoadingService
  ) { }

  ngOnInit(): void {
    if (!this.pokemon?.length) {
      this.pokemonService.fetchPokemonList(Url.INITIAL)
      this.collectPokemonService.loadCollectedPokemon();
    }
  }

  get pokemon(): Pokemon[] | undefined {
    return this.pokemonService.pokemon();
  }

  get error(): string {
    return this.pokemonService.error();
  }

  get loading(): boolean {
    return this.loadingService.loading();
  }

  public nextPage(): void {
    this.pokemonService.fetchPokemonList(Url.NEXT)
  }

  public previousPage(): void {
    this.pokemonService.fetchPokemonList(Url.PREVIOUS)
  }

}
