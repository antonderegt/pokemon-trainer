import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/_models/pokemon.model';
import { getIdAndImage } from '../../../_utils/url-parse.util'

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnInit {
  @Input() pokemon: Pokemon | undefined;
  pokemonImg: string = '';
  pokemonId: number = -1;
  pokemonName: string = '';

  constructor(private router: Router) { }

  ngOnInit(): void {
    const { image, id } = getIdAndImage(this.pokemon?.url || '')
    this.pokemonImg = image;
    this.pokemonId = id;
    this.pokemonName = this.pokemon?.name || '';
  }

  onPokemonClick() {
    this.router.navigate(['pokemon', this.pokemonId]);
  }
}
