import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon.service';
import { PokemonList } from 'src/app/_models/pokemon.model';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  @Output() nextPage: EventEmitter<any> = new EventEmitter();
  @Output() previousPage: EventEmitter<any> = new EventEmitter();

  constructor(private readonly pokemonService: PokemonService) { }

  ngOnInit(): void {
  }

  public handleNextPageClicked(): void {
    this.nextPage.emit();
  }

  public handlePreviousPageClicked(): void {
    this.previousPage.emit();
  }

  get urls(): PokemonList | undefined {
    return this.pokemonService.pokemonUrls();
  }

}
