import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http'

import { AuthCanActivateGuard } from './_guards/auth.can-activate.guard';
import { NoAuthCanActivateGuard } from './_guards/no-auth.can-activate.guard';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { HomePage } from './home/pages/home/home.page';
import { LoginComponent } from './home/components/login/login.component';

import { PokemonPage } from './pokemon/pages/pokemon/pokemon.page';
import { PokemonListComponent } from './pokemon/components/pokemon-list/pokemon-list.component';
import { PokemonCardComponent } from './pokemon/components/pokemon-card/pokemon-card.component';

import { PokemonDetailsPage } from './pokemon/pages/pokemon-details/pokemon-details.page';
import { PokemonDetailsComponent } from './pokemon/components/pokemon-details/pokemon-details.component';

import { TrainerPage } from './trainer/pages/trainer/trainer.page';
import { TrainerListComponent } from './trainer/components/trainer-list/trainer-list.component';
import { PaginationComponent } from './pokemon/components/pagination/pagination.component';

import { SpinnerOverlayComponent } from './spinner/components/spinner-overlay/spinner-overlay.component';
import { SpinnerComponent } from './spinner/components/spinner/spinner.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePage,
    LoginComponent,
    PokemonPage,
    PokemonListComponent,
    PokemonCardComponent,
    PokemonDetailsPage,
    PokemonDetailsComponent,
    TrainerPage,
    TrainerListComponent,
    PaginationComponent,
    SpinnerComponent,
    SpinnerOverlayComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    AuthCanActivateGuard,
    NoAuthCanActivateGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
