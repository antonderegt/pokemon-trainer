import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from 'src/app/_models/pokemon.model';
import { CollectPokemonService } from 'src/app/services/collect-pokemon.service';

@Component({
  selector: 'app-trainer-list',
  templateUrl: './trainer-list.component.html',
  styleUrls: ['./trainer-list.component.scss']
})
export class TrainerListComponent implements OnInit {

  constructor(
    private readonly collectPokemonService: CollectPokemonService,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
    this.collectPokemonService.loadCollectedPokemon();
  }

  get pokemon(): Pokemon[] {
    return this.collectPokemonService.collectedPokemon();
  }

  onCollectFirstPokemon() {
    this.router.navigate(['/pokemon']);
  }

}
