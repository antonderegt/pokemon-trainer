export const getIdAndImage = (url: string): any => {    
    if (url === '') return;

    const id = url.split('/').filter(Boolean).pop();
    return { id, image: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png` };
  }