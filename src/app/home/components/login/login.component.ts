import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  trainerForm = new FormGroup({
    name: new FormControl('', [
      Validators.required, 
      Validators.minLength(2),
      Validators.maxLength(20)
    ])
  });

  get name() {
    return this.trainerForm.get('name');
  }

  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit(): void {
    this.authService.login(this.trainerForm.value.name);
    this.trainerForm.reset();
    this.router.navigate(['/trainer'])
  }

  onCancel(event: Event): void {
    event.preventDefault();
    this.trainerForm.reset();
  }

}
