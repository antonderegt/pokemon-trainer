import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private _loading: boolean = true;

  constructor() { }

  public loading(): boolean {
    return this._loading;
  }

  public setLoading(loading: boolean): void {
    this._loading = loading;
  }
}
