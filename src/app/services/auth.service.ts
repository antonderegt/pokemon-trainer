import { Injectable } from '@angular/core';
import { User } from '../_models/user.model';
import { setStorage, getStorage } from '../_utils/localstorage.util';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _user: User = {
    name: '',
    isLoggedIn: false
  };

  constructor() { }

  public login(name: string): void {
    this._user = {
      name,
      isLoggedIn: true
    }
    setStorage<string>('user-session', name);
  }

  public logout(): void {
    this._user = {
      name: '',
      isLoggedIn: false
    }
    localStorage.clear();
  }

  public isLoggedIn(): boolean {
    if (this._user?.isLoggedIn) return true;

    const user = getStorage<string>('user-session');

    if (user) {
      this.login(user);
      return true;
    }

    return false;
  }

  public user(): User {
    return this._user;
  }
}
