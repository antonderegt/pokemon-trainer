import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PokemonList, Pokemon } from '../_models/pokemon.model';
import { LoadingService } from './loading.service';
import { Url } from '../_utils/url.util';

@Injectable({
    providedIn: 'root'
})
export class PokemonService {
    private pokemonList: PokemonList = {
        next: '',
        previous: '',
        results: []
    };
    private _pokemon: Pokemon[] = [];
    private _error: string = '';
    private _selectedPokemon: Pokemon | undefined;

    constructor(
        private readonly http: HttpClient,
        private readonly loadingService: LoadingService
    ) { }

    private addPokemonToSavedPokemons(pokemon: Pokemon): void {
        this._pokemon.push(pokemon);

        // Limit saved pokemon to 50, in order to keep the app fast
        if (this._pokemon.length > 50) {
            this._pokemon.shift();
        }
    }

    public fetchPokemonList(page: Url): void {
        this.loadingService.setLoading(true);

        let url: string = '';

        switch (page) {
            default:
            case Url.INITIAL:
                url = 'https://pokeapi.co/api/v2/pokemon?limit=5'
                break;
            case Url.NEXT:
                url = this.pokemonList?.next
                break;
            case Url.PREVIOUS:
                url = this.pokemonList?.previous
                break;
        }

        if (url === '' || url === null) {
            this._error = 'Page can\'t be loaded. Please try again.';
            this.loadingService.setLoading(false);
            return;
        }

        this.http.get<PokemonList>(url)
            .subscribe((pokemon: PokemonList) => {
                this.pokemonList = pokemon;
                this.loadingService.setLoading(false);
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
                this.loadingService.setLoading(false);
            })
    }

    public fetchPokemonDetailsById(id: number): void {
        this.loadingService.setLoading(true);

        this._selectedPokemon = this._pokemon.find((pokemon: Pokemon) => pokemon.id === id);

        if (this._selectedPokemon) {
            this.loadingService.setLoading(false);
            return;
        }

        const url = `https://pokeapi.co/api/v2/pokemon/${id}`;

        this.http.get<Pokemon>(url)
            .subscribe((pokemon: Pokemon) => {
                pokemon.url = url;
                this.addPokemonToSavedPokemons(pokemon);
                this._selectedPokemon = pokemon;
                this.loadingService.setLoading(false);
            }, (error: HttpErrorResponse) => {
                this._error = error.message;
                this.loadingService.setLoading(false);
            })
    }

    public error(): string {
        return this._error;
    }

    public pokemon(): Pokemon[] | undefined {
        return this.pokemonList?.results;
    }

    public pokemonUrls(): PokemonList | undefined {
        const { next, previous } = this.pokemonList
        return { next, previous };
    }

    public selectedPokemon(): Pokemon | undefined {
        return this._selectedPokemon;
    }

}