import { Injectable } from '@angular/core';
import { Pokemon } from '../_models/pokemon.model';
import { setStorage, getStorage } from '../_utils/localstorage.util';

@Injectable({
  providedIn: 'root'
})
export class CollectPokemonService {
  private _collectedPokemon: Pokemon[] = [];

  constructor() { }

  public loadCollectedPokemon(): Pokemon[] {
    const storedPokemon = getStorage<Pokemon[]>('collected-pokemon');

    if (storedPokemon) {
      this._collectedPokemon = storedPokemon;
    }

    return this._collectedPokemon;
  }

  public collectPokemon(pokemon: Pokemon | undefined): void {
    if (pokemon === undefined) return

    const { name, url } = pokemon;

    this._collectedPokemon.push({ name, url });
    setStorage<Pokemon[]>('collected-pokemon', this._collectedPokemon);
  }

  public letGoPokemon(name: string | undefined): void {
    if(name === undefined) return;

    this._collectedPokemon = this._collectedPokemon.filter((pokemon: Pokemon) => {
      return pokemon.name !== name
    })

    setStorage<Pokemon[]>('collected-pokemon', this._collectedPokemon);
  }

  public collectedPokemon(): Pokemon[] {
    return this._collectedPokemon;
  }

  public isCollected(name: string | undefined): boolean {
    if (name === undefined) return false;
    return this._collectedPokemon.some((pokemon: Pokemon) => pokemon.name === name);
  }
}
