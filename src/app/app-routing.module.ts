import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthCanActivateGuard } from './_guards/auth.can-activate.guard';
import { NoAuthCanActivateGuard } from './_guards/no-auth.can-activate.guard';

import { HomePage } from './home/pages/home/home.page';
import { PokemonPage } from './pokemon/pages/pokemon/pokemon.page';
import { PokemonDetailsPage } from './pokemon/pages/pokemon-details/pokemon-details.page';
import { TrainerPage } from './trainer/pages/trainer/trainer.page';

const routes: Routes = [
  { path: '', pathMatch: 'full', canActivate: [NoAuthCanActivateGuard], component: HomePage },
  { path: 'trainer', canActivate: [AuthCanActivateGuard], component: TrainerPage },
  { path: 'pokemon', canActivate: [AuthCanActivateGuard], component: PokemonPage },
  { path: 'pokemon/:id', canActivate: [AuthCanActivateGuard], component: PokemonDetailsPage },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
