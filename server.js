const express = require('express');
const app = express();
const port = process.env.PORT || 8080

app.use(express.static('dist/pokemon-trainer'));

app.get('/', function (req, res) {
    res.sendFile('index.html', { root: 'dist/pokemon-trainer' }
    );
});

app.listen(port, () => {
    console.log('app is running on: ', port);
});